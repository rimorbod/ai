package UserCommunication;

import java.util.Scanner;

public class UserInteractor {
    private Scanner scanner;

    public UserInteractor(Scanner scanner){
        this.scanner = scanner;
    }

    public Integer readInteger() {
        Integer value = null;
        while (value == null) {
            if (scanner.hasNextInt()) {
                value = scanner.nextInt();
            } else {
                scanner.next();
            }
        }
        return value;
    }

    public String readLineUpper(){
        return scanner.next().toUpperCase();
    }
}
