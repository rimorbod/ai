import Neuron.*;
import UserCommunication.UserInteractor;

import java.util.ArrayList;
import java.util.List;

public class ProgramManager {

    private UserInteractor interactor;
    private NeuronCreator creator;

    private List<Neuron> neurons;
    private Neuron active;

    public ProgramManager(UserInteractor interactor){
        this.interactor = interactor;
        creator = new NeuronCreator(interactor);
        neurons = new ArrayList<>();
    }

    public void start(){
        System.out.println("Witaj w programie symulującym działanie prostego neuronu");
        boolean working = true;
        while(working){
            working = work();
        }
    }

    private boolean work(){
        showInitialMenu();
        int option = interactor.readInteger();
        switch (option){
            case 0:
                return false;
            case 1:
                Neuron newNeuron = creator.createNeuron();
                addNewNeuronIfNotNull(newNeuron);
                break;
            case 2:
                listNeurons();
                break;
            case 3:
                if (neurons.isEmpty()) {
                    System.out.println("Brak neuronów");
                } else {
                    active = null;
                    chooseNeuron();
                    if (active != null){
                        actOnNeuron();
                    }
                }
                break;
            default:
                System.out.println("Nie ma takiej opcji, spróbuj ponownie");
                break;
        }
        return true;
    }

    private void actOnNeuron(){
        boolean act = true;
        while (act) {
            askForOperationOnNeuron();
            int operation = interactor.readInteger();
            switch (operation) {
                case 0:
                    act = false;
                    break;
                case 1:
                    System.out.println("To be implemented"); //TODO implement
                    break;
                case 2:
                    System.out.println(active.loadIterations() ? "Załadowano pomyślnie" : "Błąd wczytywania");
                    break;
                case 3:
                    System.out.println(active.compute() ? "Obliczenia zakończone" : "Błąd obliczeń, sprawdź dane wejściowe");
                    break;
                case 4:
                    System.out.println("To be implemented"); //TODO implement
                    break;
                case 5:
                    neurons.remove(active);
                    active = null;
                    act = false;
                    break;
                default:
                    System.out.println("Nie ma takiej opcji, spróbuj ponownie");
                    break;
            }
        }
    }

    private void askForOperationOnNeuron(){
        System.out.println("Wybierz akcję:");
        System.out.println("1) Pokaż szczegółowe dane");
        System.out.println("2) Załaduj iteracje");
        System.out.println("3) Przeprowadź symulację");
        System.out.println("4) Edytuj dane wejściowe");
        System.out.println("5) Usuń neuron");
        System.out.println("0) Wyjdź");
    }

    private void showInitialMenu(){
        System.out.println("Wybierz akcję:");
        System.out.println("1) Stwórz nowy neuron");
        System.out.println("2) Pokaż istniejące neurony");
        System.out.println("3) Wybierz istniejący neuron");
        System.out.println("0) Wyjdź");
    }

    private void chooseNeuron(){
        System.out.println("Podaj nazwę neuronu");
        String name = interactor.readLineUpper();
        if (!name.isEmpty()) {
            for (Neuron neuron : neurons) {
                if (neuron.getName().equalsIgnoreCase(name)) {
                    active = neuron;
                    return;
                }
            }
        }
    }

    private void listNeurons() {
        System.out.println("====== LISA NEURONÓW ======");
        neurons.forEach(neuron -> neuronInfoOutput(neuron));
        System.out.println("============================");
    }

    private void modifyNeurons(){
        System.out.println("Opcja chwilowo niedostępna"); //TODO implement
    }

    private void neuronInfoOutput(Neuron neuron) {
        System.out.print(neuron.toString()); //TODO implement
        System.out.println();
    }

    private void addNewNeuronIfNotNull(Neuron neuron) {
        if (neuron != null) {
            neurons.add(neuron);
        }
    }
}
