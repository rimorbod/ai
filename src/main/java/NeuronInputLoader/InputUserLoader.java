package NeuronInputLoader;

import Neuron.NeuronInput;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class InputUserLoader implements NeuronInputLoader.IInputLoader {

    private static Scanner scanner = new Scanner(System.in);
    private static Random random = new Random();

    public List<NeuronInput> loadInputs() {

        boolean go = true;
        List<NeuronInput> loadedInputs = new ArrayList<NeuronInput>();

        while(go) {

            double in = 0d;
            System.out.println("Podaj wejście");
            in = scanner.nextDouble();

            double weigth;
            System.out.println("Podaj wagę wejścia (default random)");
            weigth = scanner.hasNextDouble() ? scanner.nextDouble() : random.nextDouble();

            int output = 0;
            System.out.println("Podaj wyjście");
            output = scanner.nextInt();

            loadedInputs.add(new NeuronInput(in, weigth));

            System.out.println("Wczytać kolejne wejście? Y/N");
            String next = scanner.next().toUpperCase();
            go = next.startsWith("Y");
        }

        return loadedInputs;
    }
}
