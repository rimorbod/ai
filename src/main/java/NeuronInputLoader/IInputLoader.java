package NeuronInputLoader;

import Neuron.NeuronInput;

import java.util.List;

public interface IInputLoader {
    public List<NeuronInput> loadInputs();
}
