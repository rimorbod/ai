package Neuron;

import ActivationFunction.UnipolarStep;
import NeuronInputLoader.InputFileLoader;
import UserCommunication.UserInteractor;

public class NeuronCreator {
    private UserInteractor interactor;

    public NeuronCreator(UserInteractor interactor){
        this.interactor = interactor;
    }

    public Neuron createNeuron(){
        askForArchetype();
        int archetype = interactor.readInteger();
        switch (archetype){
            case 1:
                return createOrNeuron();
            case 2:
                return createAndNeuron();
            case 3:
                return createImplicationNeuron();
            case 4:
                return createUserNeuron();
            default:
                System.out.println("Wystąpił błąd, neuron nie został stworzony");
                return null;
        }
    }

    private Neuron createUserNeuron(){
        return null; //TODO implement
    }

    private Neuron createOrNeuron(){
        return createArchetype("OR", NeuronArchetype.OR);
    }

    private Neuron createAndNeuron(){
        return createArchetype("AND", NeuronArchetype.AND);
    }

    private Neuron createImplicationNeuron(){
        return createArchetype("IMPLIKACJA", NeuronArchetype.IMPLICATION);
    }

    private Neuron createArchetype(String name, NeuronArchetype archetype){
        return new Neuron(new UnipolarStep(), name, archetype);
    }

    private void askForArchetype(){
        System.out.println("Wybierz archetyp neuronu:");
        System.out.println("1) OR ");
        System.out.println("2) AND ");
        System.out.println("3) IMPLIKACJA ");
        System.out.println("4) INNY");
    }

    private void askForActivationFunction(){
        System.out.println("Wybierz funkcję aktywacyjną:");
        System.out.println("1) Bipolarna sigmoidalna");
        System.out.println("2) Bipolarna dyskretna");
        System.out.println("3) Unipolarna sigmoidalna");
        System.out.println("4) Unipolarna dyskretna");
    }
}
