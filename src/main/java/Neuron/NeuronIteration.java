package Neuron;

import java.util.List;

public class NeuronIteration {

    private List<NeuronInput> inputs;
    private double expectedOutput;
    private double actualOutput;
    private double bias;

    public NeuronIteration(List<NeuronInput> inputs, double expectedOutput){
        this.inputs = inputs;
        this.expectedOutput = expectedOutput;
    }
}
