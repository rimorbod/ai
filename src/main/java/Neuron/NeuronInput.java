package Neuron;

public class NeuronInput {
    private double input;
    private double weigth = 1d;

    public NeuronInput(double input, double weigth){
        this.input = input;
        this.weigth = weigth;
    }

    public NeuronInput(double input){
        this.input = input;
    }

    public void setWeigth(double weigth) {
        this.weigth = weigth;
    }

    public double getWeigth() {
        return weigth;
    }

    public double getInput() {
        return input;
    }

    @Override
    public String toString() {
        return "[X:" + input + " W:" + weigth + "]";
    }
}
