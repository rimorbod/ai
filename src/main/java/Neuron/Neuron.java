package Neuron;

import java.util.ArrayList;
import java.util.List;

import ActivationFunction.IActivationFunction;
import NeuronInputLoader.*;

public class Neuron {

    private List<NeuronIteration> iterations;
    private String name;
    private NeuronArchetype archetype;

    private IInputLoader loader;
    private IActivationFunction activationFunction;

    public Neuron(IActivationFunction function, String name, NeuronArchetype archetype){
        activationFunction = function;
        iterations = new ArrayList<>();
        this.name = name;
        this.archetype = archetype;
        switch (archetype) {
            case OR:
                loader = new InputFileLoader(Urls.URLS.OR);
                break;
            case IMPLICATION:
                loader = new InputFileLoader(Urls.URLS.OR);
                break;
            case AND:
                loader = new InputFileLoader(Urls.URLS.OR);
                break;
            case CUSTOM:
            default:
                loader = new InputUserLoader();
                break;
        }
    }

    public boolean loadIterations(){
        if (!iterations.isEmpty()) return false;
        loader.loadInputs();
        return !iterations.isEmpty();
    }

    public boolean compute(){
        return false; //TODO implement
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Neuron: " + name;
    }
}
