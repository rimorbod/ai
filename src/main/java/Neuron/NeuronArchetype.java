package Neuron;

public enum NeuronArchetype {
    OR, AND, IMPLICATION, CUSTOM
}
