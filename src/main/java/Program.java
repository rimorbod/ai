import UserCommunication.UserInteractor;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        UserInteractor interactor = new UserInteractor(scanner);
        ProgramManager manager = new ProgramManager(interactor);

        manager.start();
        scanner.close();
    }
}
