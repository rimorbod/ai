package ActivationFunction;

import ActivationFunction.IActivationFunction;

public class UnipolarSigmoid implements IActivationFunction {
    public double getNeuronOutput(double net) {
        return 1/(1+Math.exp(-net));
    }
}
