package ActivationFunction;

public interface IActivationFunction {
    double getNeuronOutput(double net);
}
