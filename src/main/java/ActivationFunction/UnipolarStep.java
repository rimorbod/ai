package ActivationFunction;

import ActivationFunction.IActivationFunction;

public class UnipolarStep implements IActivationFunction {
    public double getNeuronOutput(double net) {
        return net > 0 ? 1 : 0;
    }
}
