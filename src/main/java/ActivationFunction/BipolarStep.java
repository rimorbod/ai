package ActivationFunction;

public class BipolarStep implements IActivationFunction {
    public double getNeuronOutput(double net) {
        return net > 0 ? 1 : -1;
    }
}
