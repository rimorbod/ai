package ActivationFunction;

public class BipolarSigmoid implements IActivationFunction {
    public double getNeuronOutput(double net) {
        return (2/(1+Math.exp(-net)))-1;
    }
}
